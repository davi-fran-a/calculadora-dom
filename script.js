const n = []

function AddNota () {
  let notaInput = document.getElementById('notaInput');
  let notaTexta = document.getElementById('notaTexta');

const AddNota2 = parseFloat(notaInput.value);

if(!isNaN(AddNota2) && AddNota2 >= 0 && AddNota2 <= 10 ) {
  n.push(AddNota2);
  notaTexta.value += AddNota2 + '\n';
  notaInput.value = "";
}else {
  alert('Insira valores válidos!');
  return;
} 

function MediaFinal () {
  let MF = document.getElementById('media');
  if(n.length > 0) {
    let soma = 0;
    for (let i = 0; i < n.length; i++){
      soma += n[i];
    }
    let media = soma/n.length;
    MF.innerText = ('A media é:'+ media.toFixed(2));
  }
}
document.getElementById('Botao').addEventListener('click', MediaFinal);
}
